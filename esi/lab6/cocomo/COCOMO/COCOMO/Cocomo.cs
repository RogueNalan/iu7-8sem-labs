﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COCOMO.ActivityKind;
using Element = COCOMO.LifeCycle.Element;

namespace COCOMO
{
    public enum Mode { None, Usual, Intermediate, Embedded }

    public class Cocomo1Сalculator
    {
        #region Constants
        private const double P1_USUAL = 1.05;
        private const double P1_INTERMEDIATE = 1.12;
        private const double P1_EMBEDDED = 1.2;

        private const double P2_USUAL = 0.38;
        private const double P2_INTERMEDIATE = 0.35;
        private const double P2_EMBEDDED = 0.32;


        private const double C1_USUAL = 3.2;
        private const double C1_INTERMEDIATE = 3.0;
        private const double C1_EMBEDDED = 2.8;

        private static double C2 = 2.5;
        #endregion

        public double EAF { get; private set; }
        public double TotalWork { get; private set; }
        public double TotalTime { get; private set; }

        public ActivityKind.Array ActivityKindArray { get; private set; }
        public LifeCycle.Array LifeCycleArray { get; private set; }

        public void Compute(Mode modeType, CostDrivers drivers, int KLOC)
        {
            try
            {
                if (drivers == null)
                    throw new ArgumentNullException("Null drivers in Calculate");

                ActivityKindArray = new ActivityKind.Array();
                LifeCycleArray = new LifeCycle.Array();
                CalculateEAF(drivers);
                double C1, P1, P2;
                switch (modeType)
                {
                    case Mode.Usual:
                        C1 = C1_USUAL;
                        P1 = P1_USUAL;
                        P2 = P2_USUAL;
                        break;
                    case Mode.Intermediate:
                        C1 = C1_INTERMEDIATE;
                        P1 = P1_INTERMEDIATE;
                        P2 = P2_INTERMEDIATE;
                        break;
                    case Mode.Embedded:
                        C1 = C1_EMBEDDED;
                        P1 = P1_EMBEDDED;
                        P2 = P2_EMBEDDED;
                        break;
                    default:
                        throw new Exception("Unexpected mode!");
                }

                TotalWork = C1 * EAF * Math.Pow(KLOC, P1);
                TotalTime = C2 * Math.Pow(TotalWork, P2);

                FillActivityKindArray();
                FillLifeCycleArray();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void FillLifeCycleArray()
        {
            foreach (var elem in LifeCycleArray.Data)
            {
                elem.WorkValue = (elem.WorkPercent * TotalWork) / 100.0;
                elem.TimeValue = (elem.TimePercent * TotalTime) / 100.0;
                elem.EmployeesNumber = (int)
                    Math.Ceiling(elem.WorkValue / elem.TimeValue);
            }
        }

        public void FillActivityKindArray()
        {
            foreach (var elem in ActivityKindArray.Data)
            {
                elem.ManMonth = (TotalWork * elem.MoneyPercent) / 100.0;
                ActivityKindArray.TotalManMonth += elem.ManMonth;
            }
        }

        private void CalculateEAF(CostDrivers drivers)
        {
            EAF = 1.0;

            foreach (var p in drivers.Coefficients)
                EAF *= p.Value;
        }
    }
}
