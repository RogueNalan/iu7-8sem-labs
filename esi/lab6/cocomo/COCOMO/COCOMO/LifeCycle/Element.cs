﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COCOMO.LifeCycle
{
    public class Element
    {
        public string Name { get; set; }

        public double TimePercent { get; set; }

        public double TimeValue { get; set; }

        public double WorkPercent { get; set; }

        public double WorkValue { get; set; }

        public int EmployeesNumber { get; set; }
    }
}
