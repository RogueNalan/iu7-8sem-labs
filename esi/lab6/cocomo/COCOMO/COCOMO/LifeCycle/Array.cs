﻿
using System.Collections.Generic;

namespace COCOMO.LifeCycle
{
    public class Array
    {
        public List<Element> Data { get; set; } = new List<Element>()
        {
            new Element() { Name = "Планирование и определение требований", WorkPercent = 8.0, TimePercent = 36.0 },
            new Element() { Name = "Проектирование продукта", WorkPercent = 18.0, TimePercent = 36.0 },
            new Element() { Name = "Детальное проектирование", WorkPercent = 25.0, TimePercent = 18.0 },
            new Element() { Name = "Кодирование и тестирование отдельных модулей", WorkPercent = 26.0, TimePercent = 18.0 },
            new Element() { Name = "Интеграция и тестирование", WorkPercent = 31.0, TimePercent = 28.0 }
        };

        public double TotalTimePercent { get; set; } = 108;

        public double TotalWorkPercent { get; set; } = 136;
    }    
}
