﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COCOMO.ActivityKind
{
    public class Element
    {
        public string Name { get; set; }
        public double MoneyPercent { get; set; }
        public double ManMonth { get; set; }
    }
}
