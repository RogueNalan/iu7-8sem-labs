﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COCOMO.ActivityKind
{
    public class Array
    {
        public List<Element> Data { get; set; } = new List<Element>()
        {
            new Element() { Name = "Анализ требований", MoneyPercent = 4.0 },
            new Element() { Name = "Проектирование продукта", MoneyPercent = 12.0 },
            new Element() { Name = "Программирование", MoneyPercent = 44.0},
            new Element() { Name = "Планирование тестирования", MoneyPercent = 6.0 },
            new Element() { Name = "Верификация и аттестация", MoneyPercent = 14.0 },
            new Element() { Name = "Канцелярия проекта", MoneyPercent = 7.0 },
            new Element() { Name = "Управление конфигурацией и обеспечение качества", MoneyPercent = 7.0 },
            new Element() { Name = "Создание руководств", MoneyPercent = 6.0 }
        };

        public double TotalMoneyPercent { get; set; } = 100;

        public double TotalManMonth { get; set; } = 0;
    }
}
