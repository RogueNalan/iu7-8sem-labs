﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COCOMO
{
    public class CostDrivers
    {
        public Dictionary<string, double> Coefficients { get; } = new Dictionary<string, double>
        {
            {"RELY", 1.0},
            {"DATA", 1.0},
            {"CPLX", 1.0},
            {"TIME", 1.0},
            {"STOR", 1.0},
            {"VIRT", 1.0},
            {"TURN", 1.0},
            {"ACAP", 1.0},
            {"AEXP", 1.0},
            {"PCAP", 1.0},
            {"VEXP", 1.0},
            {"LEXP", 1.0},
            {"MODP", 1.0},
            {"TOOL", 1.0},
            {"SCED", 1.0}
        };

        private static readonly Dictionary<string, double[]> possibleValues = new Dictionary<string, double[]>
        {
            {"RELY", new []{0.75, 0.86, 1.0, 1.15, 1.4}},
            {"DATA", new []{0, 0.94, 1.0, 1.08, 1.16}},
            {"CPLX", new []{0.7, 0.85, 1.0, 1.15, 1.3}},
            {"TIME", new []{0, 0, 1.0, 1.11, 1.50}},
            {"STOR", new []{0, 0, 1.0, 1.06, 1.21}},
            {"VIRT", new []{0, 0.87, 1.0, 1.15, 1.30}},
            {"TURN", new []{0, 0.87, 1.0, 1.07, 1.15}},
            {"ACAP", new []{1.46, 1.19, 1.0, 0.86, 0.71}},
            {"AEXP", new []{1.29, 1.15, 1.0, 0.91, 0.82}},
            {"PCAP", new []{1.42, 1.17, 1.00, 0.86, 0.7}},
            {"VEXP", new []{1.21, 1.1, 1.0, 0.9, 0}},
            {"LEXP", new []{1.14, 1.07, 1.0, 0.95, 0}},
            {"MODP", new []{1.24, 1.1, 1.0, 0.91, 0.82}},
            {"TOOL", new []{1.24, 1.1, 1.0, 0.91, 0.82}},
            {"SCED", new []{1.23, 1.08, 1.0, 1.04, 1.1}}
        };

        public CostDrivers(Dictionary<string, int> lvls)
        {
            foreach (var p in lvls)
                Coefficients[p.Key] = possibleValues[p.Key][p.Value];
        }

        public double RELY { get { return Coefficients["RELY"]; } set { Coefficients["RELY"] = value; } }

        public double DATA { get { return Coefficients["DATA"]; } set { Coefficients["DATA"] = value; } }

        public double CPLX { get { return Coefficients["CPLX"]; } set { Coefficients["CPLX"] = value; } }

        public double TIME { get { return Coefficients["TIME"]; } set { Coefficients["TIME"] = value; } }

        public double STOR { get { return Coefficients["STOR"]; } set { Coefficients["STOR"] = value; } }

        public double VIRT { get { return Coefficients["VIRT"]; } set { Coefficients["VIRT"] = value; } }

        public double TURN { get { return Coefficients["TURN"]; } set { Coefficients["TURN"] = value; } }

        public double ACAP { get { return Coefficients["ACAP"]; } set { Coefficients["ACAP"] = value; } }

        public double AEXP { get { return Coefficients["AEXP"]; } set { Coefficients["AEXP"] = value; } }

        public double PCAP { get { return Coefficients["PCAP"]; } set { Coefficients["PCAP"] = value; } }

        public double VEXP { get { return Coefficients["VEXP"]; } set { Coefficients["VEXP"] = value; } }

        public double LEXP { get { return Coefficients["LEXP"]; } set { Coefficients["LEXP"] = value; } }

        public double MODP { get { return Coefficients["MODP"]; } set { Coefficients["MODP"] = value; } }

        public double TOOL { get { return Coefficients["TOOL"]; } set { Coefficients["TOOL"] = value; } }

        public double SCED { get { return Coefficients["SCED"]; } set { Coefficients["SCED"] = value; } }
    }
}
