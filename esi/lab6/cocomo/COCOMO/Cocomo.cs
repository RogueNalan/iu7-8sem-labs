﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using System.Linq;

namespace COCOMO
{
    public partial class Cocomo : Form
    {
        public Cocomo()
        {
            InitializeComponent();
        }

        private void btnCOCOMO_Click(object sender, EventArgs e)
        {
            try
            {
                Mode mode = Mode.None;
                if (rbUsual.Checked)
                    mode = Mode.Usual;
                else if (rbEmbedded.Checked)
                    mode = Mode.Embedded;
                else if (rbIntermedian.Checked)
                    mode = Mode.Intermediate;
                else
                {
                    MessageBox.Show("Unexpected mode selected!");
                    return;
                }

                var driverParams = gbControls.Controls.OfType<TrackBar>().ToDictionary(b => b.Name.Substring(2), b => b.Value);
                var drivers = new CostDrivers(driverParams);

                int KLOC = Convert.ToInt32(tbKLOC.Text);

                var calculator = new Cocomo1Сalculator();
                calculator.Compute(mode, drivers, KLOC);

                lblWork.Text = string.Format("Общ. трудозатраты: {0:0.00}", calculator.TotalWork);
                lblTime.Text = string.Format("Общ. время: {0:0.00}", calculator.TotalTime);

                FillLifeCycleGridView(calculator.LifeCycleArray);
                FillActivityKindGridView(calculator.ActivityKindArray);
                ShowEmployeesNumber(calculator.LifeCycleArray);    
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void ShowEmployeesNumber(LifeCycle.Array array)
        {
            chEmployee.Series.Clear();
            chEmployee.Series.Add("Кол-во работников");
            chEmployee.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;

            int index = 0;
            for (int i = 0; i < array.Data.Count; i++)
            {                
                int iterationLength = (int)Math.Ceiling(array.Data[i].TimeValue);
                for (int j = 0; j < iterationLength; index++, j++)
                    chEmployee.Series[0].Points.AddXY(index, array.Data[i].EmployeesNumber);
            }
           
        }

        double SummColumn(DataGridView dgv, int column)
        {
            double sum = 0;
            for (int i = 0; i < dgv.Rows.Count; ++i)
            {
                sum += Convert.ToDouble(dgv.Rows[i].Cells[column].Value);
            }
            return sum;
        }

        private void FillActivityKindGridView(ActivityKind.Array array)
        {
            dgActivityKind.Rows.Clear();
            dgActivityKind.Columns.Clear();
            dgActivityKind.ColumnCount = 3;
            dgActivityKind.RowCount = array.Data.Count + 1;

            dgActivityKind.Columns[0].Name = "Вид деятельности";
            dgActivityKind.Columns[0].Width = 200;
            dgActivityKind.Columns[1].Name = "Бюджет (%)";
            dgActivityKind.Columns[1].Width = 80;
            dgActivityKind.Columns[2].Name = "Человекомесяцы (ч*м)";
            dgActivityKind.Columns[2].Width = 80;

            for (int i = 0; i < array.Data.Count; i++)
            {
                dgActivityKind[0, i].Value = array.Data[i].Name;
                dgActivityKind[1, i].Value = string.Format("{0}", Math.Ceiling(array.Data[i].MoneyPercent));
                dgActivityKind[2, i].Value = string.Format("{0:0.0}", array.Data[i].ManMonth);
            }

            double moneySum = SummColumn(dgActivityKind, 1);
            double moneyWork = SummColumn(dgActivityKind, 2);
            dgActivityKind[0, array.Data.Count].Value = "ИТОГО:";
            dgActivityKind[1, array.Data.Count].Value = moneySum;
            dgActivityKind[2, array.Data.Count].Value = moneyWork;
        }
 
        private void FillLifeCycleGridView(LifeCycle.Array array)
        {
            dgLifeCycle.Rows.Clear();
            dgLifeCycle.Columns.Clear();
            dgLifeCycle.ColumnCount = 3;
            dgLifeCycle.RowCount = array.Data.Count+1;

            dgLifeCycle.Columns[0].Name = "Вид деятельности";
            dgLifeCycle.Columns[0].Width = 200;
            dgLifeCycle.Columns[1].Name = "Трудозатраты (ч*м)";
            dgLifeCycle.Columns[1].Width = 80;
            dgLifeCycle.Columns[2].Name = "Время (м)";
            dgLifeCycle.Columns[2].Width = 80;

            for (int i = 0; i < array.Data.Count; i++)
            {
                dgLifeCycle[0, i].Value = array.Data[i].Name;
                dgLifeCycle[1, i].Value = string.Format("{0:0.0}", array.Data[i].WorkValue);
                dgLifeCycle[2, i].Value = string.Format("{0:0.0}", array.Data[i].TimeValue);
            }

            double workSum = SummColumn(dgLifeCycle, 1);
            double timeSum = SummColumn(dgLifeCycle, 2);
            dgLifeCycle[0, array.Data.Count].Value = "ИТОГО:";
            dgLifeCycle[1, array.Data.Count].Value = workSum;
            dgLifeCycle[2, array.Data.Count].Value = timeSum;
        }

        private void trackBar_ValueChanged(object sender, EventArgs e)
        {
            var t = (TrackBar)sender;
            if (t == trTIME || t == trSTOR)
            {
                if (t.Value < 2)
                    t.Value = 2;
            }
            else if (t == trVIRT || t == trTURN)
            {
                if (t.Value < 1)
                    t.Value = 1;
            }
            else if (t == trVEXP || t == trLEXP)
            {
                if (t.Value > 3)
                    t.Value = 3;
            }
        }

        private void trackBar_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }
    }
}
