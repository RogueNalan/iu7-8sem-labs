﻿namespace COCOMO
{
    partial class Cocomo
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.rbUsual = new System.Windows.Forms.RadioButton();
            this.rbIntermedian = new System.Windows.Forms.RadioButton();
            this.rbEmbedded = new System.Windows.Forms.RadioButton();
            this.btnCOCOMO = new System.Windows.Forms.Button();
            this.dgLifeCycle = new System.Windows.Forms.DataGridView();
            this.dgActivityKind = new System.Windows.Forms.DataGridView();
            this.gbControls = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.trSCED = new System.Windows.Forms.TrackBar();
            this.trLEXP = new System.Windows.Forms.TrackBar();
            this.trVIRT = new System.Windows.Forms.TrackBar();
            this.trTOOL = new System.Windows.Forms.TrackBar();
            this.tbKLOC = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.trVEXP = new System.Windows.Forms.TrackBar();
            this.trSTOR = new System.Windows.Forms.TrackBar();
            this.trMODP = new System.Windows.Forms.TrackBar();
            this.trPCAP = new System.Windows.Forms.TrackBar();
            this.trTIME = new System.Windows.Forms.TrackBar();
            this.trAEXP = new System.Windows.Forms.TrackBar();
            this.trCPLX = new System.Windows.Forms.TrackBar();
            this.trACAP = new System.Windows.Forms.TrackBar();
            this.trDATA = new System.Windows.Forms.TrackBar();
            this.trTURN = new System.Windows.Forms.TrackBar();
            this.trRELY = new System.Windows.Forms.TrackBar();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.chEmployee = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblWork = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgLifeCycle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgActivityKind)).BeginInit();
            this.gbControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trSCED)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trLEXP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trVIRT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trTOOL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trVEXP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trSTOR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trMODP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPCAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trTIME)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trAEXP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trCPLX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trACAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trDATA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trTURN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trRELY)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chEmployee)).BeginInit();
            this.SuspendLayout();
            // 
            // rbUsual
            // 
            this.rbUsual.AutoSize = true;
            this.rbUsual.Location = new System.Drawing.Point(6, 19);
            this.rbUsual.Name = "rbUsual";
            this.rbUsual.Size = new System.Drawing.Size(72, 17);
            this.rbUsual.TabIndex = 7;
            this.rbUsual.Text = "Обычный";
            this.rbUsual.UseVisualStyleBackColor = true;
            // 
            // rbIntermedian
            // 
            this.rbIntermedian.AutoSize = true;
            this.rbIntermedian.Checked = true;
            this.rbIntermedian.Location = new System.Drawing.Point(6, 42);
            this.rbIntermedian.Name = "rbIntermedian";
            this.rbIntermedian.Size = new System.Drawing.Size(108, 17);
            this.rbIntermedian.TabIndex = 8;
            this.rbIntermedian.TabStop = true;
            this.rbIntermedian.Text = "Промежуточный";
            this.rbIntermedian.UseVisualStyleBackColor = true;
            // 
            // rbEmbedded
            // 
            this.rbEmbedded.AutoSize = true;
            this.rbEmbedded.Location = new System.Drawing.Point(6, 65);
            this.rbEmbedded.Name = "rbEmbedded";
            this.rbEmbedded.Size = new System.Drawing.Size(87, 17);
            this.rbEmbedded.TabIndex = 9;
            this.rbEmbedded.Text = "Встроенный";
            this.rbEmbedded.UseVisualStyleBackColor = true;
            // 
            // btnCOCOMO
            // 
            this.btnCOCOMO.Location = new System.Drawing.Point(674, 48);
            this.btnCOCOMO.Name = "btnCOCOMO";
            this.btnCOCOMO.Size = new System.Drawing.Size(118, 23);
            this.btnCOCOMO.TabIndex = 10;
            this.btnCOCOMO.Text = "Рассчитать";
            this.btnCOCOMO.UseVisualStyleBackColor = true;
            this.btnCOCOMO.Click += new System.EventHandler(this.btnCOCOMO_Click);
            // 
            // dgLifeCycle
            // 
            this.dgLifeCycle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgLifeCycle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLifeCycle.Location = new System.Drawing.Point(3, 3);
            this.dgLifeCycle.Name = "dgLifeCycle";
            this.dgLifeCycle.RowHeadersVisible = false;
            this.dgLifeCycle.Size = new System.Drawing.Size(372, 239);
            this.dgLifeCycle.TabIndex = 13;
            // 
            // dgActivityKind
            // 
            this.dgActivityKind.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgActivityKind.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgActivityKind.Location = new System.Drawing.Point(3, 3);
            this.dgActivityKind.Name = "dgActivityKind";
            this.dgActivityKind.RowHeadersVisible = false;
            this.dgActivityKind.Size = new System.Drawing.Size(372, 239);
            this.dgActivityKind.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.gbControls.Controls.Add(this.panel5);
            this.gbControls.Controls.Add(this.panel4);
            this.gbControls.Controls.Add(this.panel3);
            this.gbControls.Controls.Add(this.panel2);
            this.gbControls.Controls.Add(this.panel1);
            this.gbControls.Controls.Add(this.trSCED);
            this.gbControls.Controls.Add(this.trLEXP);
            this.gbControls.Controls.Add(this.trVIRT);
            this.gbControls.Controls.Add(this.trTOOL);
            this.gbControls.Controls.Add(this.tbKLOC);
            this.gbControls.Controls.Add(this.label37);
            this.gbControls.Controls.Add(this.trVEXP);
            this.gbControls.Controls.Add(this.trSTOR);
            this.gbControls.Controls.Add(this.trMODP);
            this.gbControls.Controls.Add(this.trPCAP);
            this.gbControls.Controls.Add(this.trTIME);
            this.gbControls.Controls.Add(this.trAEXP);
            this.gbControls.Controls.Add(this.trCPLX);
            this.gbControls.Controls.Add(this.trACAP);
            this.gbControls.Controls.Add(this.trDATA);
            this.gbControls.Controls.Add(this.trTURN);
            this.gbControls.Controls.Add(this.trRELY);
            this.gbControls.Controls.Add(this.label36);
            this.gbControls.Controls.Add(this.label35);
            this.gbControls.Controls.Add(this.label21);
            this.gbControls.Controls.Add(this.label34);
            this.gbControls.Controls.Add(this.label20);
            this.gbControls.Controls.Add(this.label33);
            this.gbControls.Controls.Add(this.label19);
            this.gbControls.Controls.Add(this.label32);
            this.gbControls.Controls.Add(this.label18);
            this.gbControls.Controls.Add(this.label31);
            this.gbControls.Controls.Add(this.label17);
            this.gbControls.Controls.Add(this.label30);
            this.gbControls.Controls.Add(this.label16);
            this.gbControls.Controls.Add(this.label29);
            this.gbControls.Controls.Add(this.label15);
            this.gbControls.Controls.Add(this.label28);
            this.gbControls.Controls.Add(this.label14);
            this.gbControls.Controls.Add(this.label27);
            this.gbControls.Controls.Add(this.label13);
            this.gbControls.Controls.Add(this.label26);
            this.gbControls.Controls.Add(this.label12);
            this.gbControls.Controls.Add(this.label25);
            this.gbControls.Controls.Add(this.label11);
            this.gbControls.Controls.Add(this.label24);
            this.gbControls.Controls.Add(this.label10);
            this.gbControls.Controls.Add(this.label23);
            this.gbControls.Controls.Add(this.label9);
            this.gbControls.Controls.Add(this.label22);
            this.gbControls.Controls.Add(this.label7);
            this.gbControls.Controls.Add(this.label3);
            this.gbControls.Controls.Add(this.label4);
            this.gbControls.Controls.Add(this.label2);
            this.gbControls.Controls.Add(this.label5);
            this.gbControls.Controls.Add(this.label1);
            this.gbControls.Controls.Add(this.label8);
            this.gbControls.Location = new System.Drawing.Point(12, 12);
            this.gbControls.Name = "groupBox1";
            this.gbControls.Size = new System.Drawing.Size(509, 409);
            this.gbControls.TabIndex = 17;
            this.gbControls.TabStop = false;
            this.gbControls.Text = "Cost drivers:";
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(430, 323);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(47, 13);
            this.panel5.TabIndex = 20;
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(430, 301);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(47, 13);
            this.panel4.TabIndex = 20;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(291, 191);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(47, 13);
            this.panel3.TabIndex = 20;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(291, 169);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(91, 13);
            this.panel2.TabIndex = 20;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(291, 147);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(91, 13);
            this.panel1.TabIndex = 20;
            // 
            // trSCED
            // 
            this.trSCED.AutoSize = false;
            this.trSCED.LargeChange = 1;
            this.trSCED.Location = new System.Drawing.Point(291, 389);
            this.trSCED.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trSCED.Maximum = 4;
            this.trSCED.Name = "trSCED";
            this.trSCED.Size = new System.Drawing.Size(186, 13);
            this.trSCED.TabIndex = 3;
            this.trSCED.Value = 3;
            this.trSCED.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trSCED.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trLEXP
            // 
            this.trLEXP.AutoSize = false;
            this.trLEXP.LargeChange = 1;
            this.trLEXP.Location = new System.Drawing.Point(291, 323);
            this.trLEXP.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trLEXP.Maximum = 4;
            this.trLEXP.Name = "trLEXP";
            this.trLEXP.Size = new System.Drawing.Size(186, 13);
            this.trLEXP.TabIndex = 3;
            this.trLEXP.Value = 2;
            this.trLEXP.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trLEXP.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trVIRT
            // 
            this.trVIRT.AutoSize = false;
            this.trVIRT.LargeChange = 1;
            this.trVIRT.Location = new System.Drawing.Point(291, 191);
            this.trVIRT.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trVIRT.Maximum = 4;
            this.trVIRT.Name = "trVIRT";
            this.trVIRT.Size = new System.Drawing.Size(186, 13);
            this.trVIRT.TabIndex = 3;
            this.trVIRT.Value = 2;
            this.trVIRT.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trVIRT.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trTOOL
            // 
            this.trTOOL.AutoSize = false;
            this.trTOOL.LargeChange = 1;
            this.trTOOL.Location = new System.Drawing.Point(291, 367);
            this.trTOOL.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trTOOL.Maximum = 4;
            this.trTOOL.Name = "trTOOL";
            this.trTOOL.Size = new System.Drawing.Size(186, 13);
            this.trTOOL.TabIndex = 3;
            this.trTOOL.Value = 2;
            this.trTOOL.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trTOOL.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // tbKLOC
            // 
            this.tbKLOC.Location = new System.Drawing.Point(182, 16);
            this.tbKLOC.Name = "tbKLOC";
            this.tbKLOC.Size = new System.Drawing.Size(58, 20);
            this.tbKLOC.TabIndex = 19;
            this.tbKLOC.Text = "430";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(15, 19);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(161, 13);
            this.label37.TabIndex = 18;
            this.label37.Text = "Количество строк кода (kLOC)";
            // 
            // trVEXP
            // 
            this.trVEXP.AutoSize = false;
            this.trVEXP.LargeChange = 1;
            this.trVEXP.Location = new System.Drawing.Point(291, 301);
            this.trVEXP.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trVEXP.Maximum = 4;
            this.trVEXP.Name = "trVEXP";
            this.trVEXP.Size = new System.Drawing.Size(186, 13);
            this.trVEXP.TabIndex = 3;
            this.trVEXP.Value = 2;
            this.trVEXP.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trVEXP.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trSTOR
            // 
            this.trSTOR.AutoSize = false;
            this.trSTOR.LargeChange = 1;
            this.trSTOR.Location = new System.Drawing.Point(291, 169);
            this.trSTOR.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trSTOR.Maximum = 4;
            this.trSTOR.Name = "trSTOR";
            this.trSTOR.Size = new System.Drawing.Size(186, 13);
            this.trSTOR.TabIndex = 3;
            this.trSTOR.Value = 2;
            this.trSTOR.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trSTOR.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trMODP
            // 
            this.trMODP.AutoSize = false;
            this.trMODP.LargeChange = 1;
            this.trMODP.Location = new System.Drawing.Point(291, 345);
            this.trMODP.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trMODP.Maximum = 4;
            this.trMODP.Name = "trMODP";
            this.trMODP.Size = new System.Drawing.Size(186, 13);
            this.trMODP.TabIndex = 3;
            this.trMODP.Value = 2;
            this.trMODP.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trMODP.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trPCAP
            // 
            this.trPCAP.AutoSize = false;
            this.trPCAP.LargeChange = 1;
            this.trPCAP.Location = new System.Drawing.Point(291, 279);
            this.trPCAP.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trPCAP.Maximum = 4;
            this.trPCAP.Name = "trPCAP";
            this.trPCAP.Size = new System.Drawing.Size(186, 13);
            this.trPCAP.TabIndex = 3;
            this.trPCAP.Value = 3;
            this.trPCAP.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trPCAP.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trTIME
            // 
            this.trTIME.AutoSize = false;
            this.trTIME.LargeChange = 1;
            this.trTIME.Location = new System.Drawing.Point(291, 147);
            this.trTIME.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trTIME.Maximum = 4;
            this.trTIME.Name = "trTIME";
            this.trTIME.Size = new System.Drawing.Size(186, 13);
            this.trTIME.TabIndex = 3;
            this.trTIME.Value = 3;
            this.trTIME.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trTIME.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trAEXP
            // 
            this.trAEXP.AutoSize = false;
            this.trAEXP.LargeChange = 1;
            this.trAEXP.Location = new System.Drawing.Point(291, 257);
            this.trAEXP.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trAEXP.Maximum = 4;
            this.trAEXP.Name = "trAEXP";
            this.trAEXP.Size = new System.Drawing.Size(186, 13);
            this.trAEXP.TabIndex = 3;
            this.trAEXP.Value = 2;
            this.trAEXP.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trAEXP.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trCPLX
            // 
            this.trCPLX.AutoSize = false;
            this.trCPLX.LargeChange = 1;
            this.trCPLX.Location = new System.Drawing.Point(291, 125);
            this.trCPLX.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trCPLX.Maximum = 4;
            this.trCPLX.Name = "trCPLX";
            this.trCPLX.Size = new System.Drawing.Size(186, 13);
            this.trCPLX.TabIndex = 3;
            this.trCPLX.Value = 2;
            this.trCPLX.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trCPLX.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trACAP
            // 
            this.trACAP.AutoSize = false;
            this.trACAP.LargeChange = 1;
            this.trACAP.Location = new System.Drawing.Point(291, 235);
            this.trACAP.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trACAP.Maximum = 4;
            this.trACAP.Name = "trACAP";
            this.trACAP.Size = new System.Drawing.Size(186, 13);
            this.trACAP.TabIndex = 3;
            this.trACAP.Value = 3;
            this.trACAP.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trACAP.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trDATA
            // 
            this.trDATA.AutoSize = false;
            this.trDATA.LargeChange = 1;
            this.trDATA.Location = new System.Drawing.Point(291, 103);
            this.trDATA.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trDATA.Maximum = 4;
            this.trDATA.Name = "trDATA";
            this.trDATA.Size = new System.Drawing.Size(186, 13);
            this.trDATA.TabIndex = 3;
            this.trDATA.Value = 2;
            this.trDATA.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trDATA.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trTURN
            // 
            this.trTURN.AutoSize = false;
            this.trTURN.LargeChange = 1;
            this.trTURN.Location = new System.Drawing.Point(291, 213);
            this.trTURN.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trTURN.Maximum = 4;
            this.trTURN.Name = "trTURN";
            this.trTURN.Size = new System.Drawing.Size(186, 13);
            this.trTURN.TabIndex = 3;
            this.trTURN.Value = 2;
            this.trTURN.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trTURN.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // trRELY
            // 
            this.trRELY.AutoSize = false;
            this.trRELY.LargeChange = 1;
            this.trRELY.Location = new System.Drawing.Point(291, 81);
            this.trRELY.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.trRELY.Maximum = 4;
            this.trRELY.Name = "trRELY";
            this.trRELY.Size = new System.Drawing.Size(186, 13);
            this.trRELY.TabIndex = 3;
            this.trRELY.Value = 3;
            this.trRELY.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            this.trRELY.Validating += new System.ComponentModel.CancelEventHandler(this.trackBar_Validating);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 389);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(160, 13);
            this.label36.TabIndex = 2;
            this.label36.Text = "Требуемые сроки разработки";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 367);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(234, 13);
            this.label35.TabIndex = 2;
            this.label35.Text = "Использование программных инструментов";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(246, 389);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(36, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "SCED";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 345);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(205, 13);
            this.label34.TabIndex = 2;
            this.label34.Text = "Использование современных методов";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(246, 367);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "TOOL";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 323);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(181, 13);
            this.label33.TabIndex = 2;
            this.label33.Text = "Знание языка программирования";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(246, 345);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "MODP";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 301);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(156, 13);
            this.label32.TabIndex = 2;
            this.label32.Text = "Знание виртуальной машины";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(246, 323);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "LEXP";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 279);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(150, 13);
            this.label31.TabIndex = 2;
            this.label31.Text = "Способности программиста";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(246, 301);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "VEXP";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 257);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(109, 13);
            this.label30.TabIndex = 2;
            this.label30.Text = "Знание приложений";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(246, 279);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "PCAP";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 235);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(129, 13);
            this.label29.TabIndex = 2;
            this.label29.Text = "Способности аналитика";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(246, 257);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "AEXP";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 213);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(151, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "Время реакции компьютера";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(246, 235);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "ACAP";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 191);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(193, 13);
            this.label27.TabIndex = 2;
            this.label27.Text = "Изменчивость виртуальной машины";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(246, 213);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "TURN";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 169);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(206, 13);
            this.label26.TabIndex = 2;
            this.label26.Text = "Ограничение объема основной памяти";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(246, 191);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "VIRT";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 147);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(185, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "Ограничение времени выполнения";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(246, 169);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "STOR";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 125);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(112, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Сложность продукта";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(246, 147);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "TIME";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 103);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(115, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Размер базы данных";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(246, 125);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "CPLX";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 81);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(127, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "Требуемая надежность";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(246, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "DATA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(367, 56);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Норм.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(414, 56);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Выс.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(326, 56);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Низ.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(455, 56);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Оч.выс.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(273, 56);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Оч.низ.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(246, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "RELY";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbUsual);
            this.groupBox2.Controls.Add(this.rbIntermedian);
            this.groupBox2.Controls.Add(this.rbEmbedded);
            this.groupBox2.Location = new System.Drawing.Point(527, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(118, 90);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Тип режима:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(527, 150);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(386, 271);
            this.tabControl1.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgLifeCycle);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(378, 245);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Работа+Время";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgActivityKind);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(378, 245);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Тр.з. ЖЦ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.chEmployee);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(378, 245);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Работники";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // chEmployee
            // 
            this.chEmployee.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.chEmployee.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chEmployee.Legends.Add(legend2);
            this.chEmployee.Location = new System.Drawing.Point(3, 3);
            this.chEmployee.Name = "chEmployee";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chEmployee.Series.Add(series2);
            this.chEmployee.Size = new System.Drawing.Size(372, 239);
            this.chEmployee.TabIndex = 18;
            this.chEmployee.Text = "Количество сотрудников";
            // 
            // lblWork
            // 
            this.lblWork.AutoSize = true;
            this.lblWork.Location = new System.Drawing.Point(531, 120);
            this.lblWork.Name = "lblWork";
            this.lblWork.Size = new System.Drawing.Size(112, 13);
            this.lblWork.TabIndex = 14;
            this.lblWork.Text = "Общ. трудозатраты: ";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(743, 120);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(74, 13);
            this.lblTime.TabIndex = 14;
            this.lblTime.Text = "Общ. время: ";
            // 
            // Cocomo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 433);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblWork);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gbControls);
            this.Controls.Add(this.btnCOCOMO);
            this.Name = "Cocomo";
            this.Text = "COCOMO";
            ((System.ComponentModel.ISupportInitialize)(this.dgLifeCycle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgActivityKind)).EndInit();
            this.gbControls.ResumeLayout(false);
            this.gbControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trSCED)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trLEXP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trVIRT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trTOOL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trVEXP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trSTOR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trMODP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPCAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trTIME)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trAEXP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trCPLX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trACAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trDATA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trTURN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trRELY)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chEmployee)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbUsual;
        private System.Windows.Forms.RadioButton rbIntermedian;
        private System.Windows.Forms.RadioButton rbEmbedded;
        private System.Windows.Forms.Button btnCOCOMO;
        private System.Windows.Forms.DataGridView dgLifeCycle;
        private System.Windows.Forms.DataGridView dgActivityKind;
        private System.Windows.Forms.GroupBox gbControls;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbKLOC;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chEmployee;
        private System.Windows.Forms.Label lblWork;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.TrackBar trSCED;
        private System.Windows.Forms.TrackBar trLEXP;
        private System.Windows.Forms.TrackBar trVIRT;
        private System.Windows.Forms.TrackBar trTOOL;
        private System.Windows.Forms.TrackBar trVEXP;
        private System.Windows.Forms.TrackBar trSTOR;
        private System.Windows.Forms.TrackBar trMODP;
        private System.Windows.Forms.TrackBar trPCAP;
        private System.Windows.Forms.TrackBar trTIME;
        private System.Windows.Forms.TrackBar trAEXP;
        private System.Windows.Forms.TrackBar trCPLX;
        private System.Windows.Forms.TrackBar trACAP;
        private System.Windows.Forms.TrackBar trDATA;
        private System.Windows.Forms.TrackBar trTURN;
        private System.Windows.Forms.TrackBar trRELY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
    }
}

